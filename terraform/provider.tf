terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "s3" {
    bucket = "t-iac-backend"
    region = "us-east-1"
    key    = "terraform.tfstate"
  }
}

# Configure and downloading plugins for aws
provider "aws" {
  region     = "${var.aws_region}"
}

